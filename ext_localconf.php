<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'KITT3N.Kitt3nMap',
            'Kitt3nmaprender',
            [
                'Map' => 'render'
            ],
            // non-cacheable actions
            [
                'Map' => '',
                'Item' => ''
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    kitt3nmaprender {
                        iconIdentifier = kitt3n_map-plugin-kitt3nmaprender
                        title = LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_map_kitt3nmaprender.name
                        description = LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3n_map_kitt3nmaprender.description
                        tt_content_defValues {
                            CType = list
                            list_type = kitt3nmap_kitt3nmaprender
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);
		
			$iconRegistry->registerIcon(
				'kitt3n_map-plugin-kitt3nmaprender',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:kitt3n_map/Resources/Public/Icons/user_plugin_kitt3nmaprender.svg']
			);
		
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder
call_user_func(
    function ($extKey, $globals) {

        // wizards
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.kitt3nmaprender >
                wizards.newContentElement.wizardItems {
                    KITT3N {
                        header = KITT3N
                        after = common,special,menu,plugins,forms
                        elements {
                            kitt3nmaprender {
                                iconIdentifier = kitt3n_svg_big
                                title = kitt3n | Map
                                description = Shows a google map with marker or polygons in the frontend.
                                tt_content_defValues {
                                    CType = list
                                    list_type = kitt3nmap_kitt3nmaprender
                                }
                            }
                        }
                        show := addToList(kitt3nmaprender)
                    }

                }
            }'
        );


        // Register for hook to show preview of tt_content element of CType="yourextensionkey_newcontentelement" in page module
        $globals['TYPO3_CONF_VARS']['SC_OPTIONS']['cms/layout/class.tx_cms_layout.php']['tt_content_drawItem'][$extKey] =
            \KITT3N\Kitt3nMap\Hooks\Previews\MapPreviewRenderer::class;

    }, $_EXTKEY, $GLOBALS
);


///** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
//$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
//
//$signalSlotDispatcher->connect(
//    \KITT3N\Kitt3nMap\Controller\MapController::class,
//    'afterPolygonArrayPhpSignal',
//    \KITT3N\Kitt3nMap\Slot\MapControllerSlot::class,
//    'afterPolygonArrayPhpSlot',
//    FALSE
//);