<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'KITT3N.Kitt3nMap',
            'Kitt3nmaprender',
            'Kitt3n Map Render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('kitt3n_map', 'Configuration/TypoScript', 'kitt3n | Map');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nmap_domain_model_map', 'EXT:kitt3n_map/Resources/Private/Language/locallang_csh_tx_kitt3nmap_domain_model_map.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nmap_domain_model_map');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_kitt3nmap_domain_model_item', 'EXT:kitt3n_map/Resources/Private/Language/locallang_csh_tx_kitt3nmap_domain_model_item.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_kitt3nmap_domain_model_item');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder