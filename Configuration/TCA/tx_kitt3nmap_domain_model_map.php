<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map',
        'label' => 'name',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'name,width,height,lat,lon,language,type,controlsize,controlzoompos,controltypepos,controltypestyle,controlstreetviewpos,controlrotatepos,controlfullscreenpos,mcstyles',
        'iconfile' => 'EXT:kitt3n_map/Resources/Public/Icons/tx_kitt3nmap_domain_model_map.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, stylefile, width, widthunit, height, heightunit, fitbounds, lat, lon, zoom, minzoom, maxzoom, language, type, tilt, controls, controlsize, controlzoom, controlzoompos, controltype, controltypepos, controltypestyle, controlstreetview, controlstreetviewpos, controlrotate, controlrotatepos, controlscale, controlfullscreen, controlfullscreenpos, panning, keyboardshortcuts, doubleclickzoom, scrollzoom, clickablepoints, mc, mctype, mcgridsize, mcmaxzoom, mczoomonclick, mcaveragecenter, mcminclustersize, mcstyles, item',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, stylefile, width, widthunit, height, heightunit, fitbounds, lat, lon, zoom, minzoom, maxzoom, language, type, tilt, controls, controlsize, controlzoom, controlzoompos, controltype, controltypepos, controltypestyle, controlstreetview, controlstreetviewpos, controlrotate, controlrotatepos, controlscale, controlfullscreen, controlfullscreenpos, panning, keyboardshortcuts, doubleclickzoom, scrollzoom, clickablepoints, mc, mctype, mcgridsize, mcmaxzoom, mczoomonclick, mcaveragecenter, mcminclustersize, mcstyles, item, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'default' => 0,
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_kitt3nmap_domain_model_map',
                'foreign_table_where' => 'AND {#tx_kitt3nmap_domain_model_map}.{#pid}=###CURRENT_PID### AND {#tx_kitt3nmap_domain_model_map}.{#sys_language_uid} IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.visible',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'renderType' => 'inputDateTime',
                'eval' => 'datetime,int',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
                'behaviour' => [
                    'allowLanguageSynchronization' => true
                ]
            ],
        ],

        'name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'stylefile' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.stylefile',
            'config' => \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::getFileFieldTCAConfig(
                'stylefile',
                [
                    'appearance' => [
                        'createNewRelationLinkTitle' => 'LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:media.addFileReference'
                    ],
                    'foreign_types' => [
                        '0' => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_TEXT => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_IMAGE => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_AUDIO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_VIDEO => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ],
                        \TYPO3\CMS\Core\Resource\File::FILETYPE_APPLICATION => [
                            'showitem' => '
                            --palette--;LLL:EXT:lang/locallang_tca.xlf:sys_file_reference.imageoverlayPalette;imageoverlayPalette,
                            --palette--;;filePalette'
                        ]
                    ],
                    'maxitems' => 1
                ],
                'json'
            ),
        ],
        'width' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.width',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'widthunit' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.widthunit',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'height' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.height',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'heightunit' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.heightunit',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['-- Label --', 0],
                ],
                'size' => 1,
                'maxitems' => 1,
                'eval' => ''
            ],
        ],
        'fitbounds' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.fitbounds',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'lat' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.lat',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'lon' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.lon',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'zoom' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.zoom',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'minzoom' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.minzoom',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'maxzoom' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.maxzoom',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'language' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.language',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'type' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.type',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'tilt' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.tilt',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'controls' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controls',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'controlsize' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlsize',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'controlzoom' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlzoom',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'controlzoompos' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlzoompos',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'controltype' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controltype',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'controltypepos' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controltypepos',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'controltypestyle' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controltypestyle',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'controlstreetview' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlstreetview',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'controlstreetviewpos' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlstreetviewpos',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'controlrotate' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlrotate',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'controlrotatepos' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlrotatepos',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'controlscale' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlscale',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'controlfullscreen' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlfullscreen',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'controlfullscreenpos' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.controlfullscreenpos',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'panning' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.panning',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'keyboardshortcuts' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.keyboardshortcuts',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'doubleclickzoom' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.doubleclickzoom',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'scrollzoom' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.scrollzoom',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'clickablepoints' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.clickablepoints',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'mc' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mc',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'mctype' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mctype',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'mcgridsize' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mcgridsize',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'mcmaxzoom' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mcmaxzoom',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'mczoomonclick' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mczoomonclick',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'mcaveragecenter' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mcaveragecenter',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
                'default' => 0,
            ]
        ],
        'mcminclustersize' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mcminclustersize',
            'config' => [
                'type' => 'input',
                'size' => 4,
                'eval' => 'int'
            ]
        ],
        'mcstyles' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.mcstyles',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'item' => [
            'exclude' => false,
            'label' => 'LLL:EXT:kitt3n_map/Resources/Private/Language/locallang_db.xlf:tx_kitt3nmap_domain_model_map.item',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_kitt3nmap_domain_model_item',
                'MM' => 'tx_kitt3nmap_map_item_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'fieldControl' => [
                    'editPopup' => [
                        'disabled' => false,
                    ],
                    'addRecord' => [
                        'disabled' => false,
                    ],
                    'listModule' => [
                        'disabled' => true,
                    ],
                ],
            ],
            
        ],
    
    ],
];
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder