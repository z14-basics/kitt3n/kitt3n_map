<?php
namespace KITT3N\Kitt3nMap\Tests\Unit\Domain\Model;

/**
 * Test case.
 *
 * @author Oliver Merz <o.merz@kitt3n.de>
 * @author Georg Kathan <g.kathan@kitt3n.de>
 * @author Dominik Hilser <d.hilser@kitt3n.de>
 */
class MapTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nMap\Domain\Model\Map
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \KITT3N\Kitt3nMap\Domain\Model\Map();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getStylefileReturnsInitialValueForFileReference()
    {
        self::assertEquals(
            null,
            $this->subject->getStylefile()
        );
    }

    /**
     * @test
     */
    public function setStylefileForFileReferenceSetsStylefile()
    {
        $fileReferenceFixture = new \TYPO3\CMS\Extbase\Domain\Model\FileReference();
        $this->subject->setStylefile($fileReferenceFixture);

        self::assertAttributeEquals(
            $fileReferenceFixture,
            'stylefile',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWidthReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getWidth()
        );
    }

    /**
     * @test
     */
    public function setWidthForStringSetsWidth()
    {
        $this->subject->setWidth('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'width',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getWidthunitReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getWidthunit()
        );
    }

    /**
     * @test
     */
    public function setWidthunitForIntSetsWidthunit()
    {
        $this->subject->setWidthunit(12);

        self::assertAttributeEquals(
            12,
            'widthunit',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHeightReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHeight()
        );
    }

    /**
     * @test
     */
    public function setHeightForStringSetsHeight()
    {
        $this->subject->setHeight('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'height',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHeightunitReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getHeightunit()
        );
    }

    /**
     * @test
     */
    public function setHeightunitForIntSetsHeightunit()
    {
        $this->subject->setHeightunit(12);

        self::assertAttributeEquals(
            12,
            'heightunit',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFitboundsReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getFitbounds()
        );
    }

    /**
     * @test
     */
    public function setFitboundsForBoolSetsFitbounds()
    {
        $this->subject->setFitbounds(true);

        self::assertAttributeEquals(
            true,
            'fitbounds',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLatReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLat()
        );
    }

    /**
     * @test
     */
    public function setLatForStringSetsLat()
    {
        $this->subject->setLat('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lat',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLonReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLon()
        );
    }

    /**
     * @test
     */
    public function setLonForStringSetsLon()
    {
        $this->subject->setLon('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'lon',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getZoomReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getZoom()
        );
    }

    /**
     * @test
     */
    public function setZoomForIntSetsZoom()
    {
        $this->subject->setZoom(12);

        self::assertAttributeEquals(
            12,
            'zoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMinzoomReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMinzoom()
        );
    }

    /**
     * @test
     */
    public function setMinzoomForIntSetsMinzoom()
    {
        $this->subject->setMinzoom(12);

        self::assertAttributeEquals(
            12,
            'minzoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMaxzoomReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMaxzoom()
        );
    }

    /**
     * @test
     */
    public function setMaxzoomForIntSetsMaxzoom()
    {
        $this->subject->setMaxzoom(12);

        self::assertAttributeEquals(
            12,
            'maxzoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getLanguageReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getLanguage()
        );
    }

    /**
     * @test
     */
    public function setLanguageForStringSetsLanguage()
    {
        $this->subject->setLanguage('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'language',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getType()
        );
    }

    /**
     * @test
     */
    public function setTypeForStringSetsType()
    {
        $this->subject->setType('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'type',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTiltReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getTilt()
        );
    }

    /**
     * @test
     */
    public function setTiltForIntSetsTilt()
    {
        $this->subject->setTilt(12);

        self::assertAttributeEquals(
            12,
            'tilt',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlsReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getControls()
        );
    }

    /**
     * @test
     */
    public function setControlsForIntSetsControls()
    {
        $this->subject->setControls(12);

        self::assertAttributeEquals(
            12,
            'controls',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlsizeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getControlsize()
        );
    }

    /**
     * @test
     */
    public function setControlsizeForStringSetsControlsize()
    {
        $this->subject->setControlsize('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'controlsize',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlzoomReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getControlzoom()
        );
    }

    /**
     * @test
     */
    public function setControlzoomForBoolSetsControlzoom()
    {
        $this->subject->setControlzoom(true);

        self::assertAttributeEquals(
            true,
            'controlzoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlzoomposReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getControlzoompos()
        );
    }

    /**
     * @test
     */
    public function setControlzoomposForStringSetsControlzoompos()
    {
        $this->subject->setControlzoompos('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'controlzoompos',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControltypeReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getControltype()
        );
    }

    /**
     * @test
     */
    public function setControltypeForBoolSetsControltype()
    {
        $this->subject->setControltype(true);

        self::assertAttributeEquals(
            true,
            'controltype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControltypeposReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getControltypepos()
        );
    }

    /**
     * @test
     */
    public function setControltypeposForStringSetsControltypepos()
    {
        $this->subject->setControltypepos('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'controltypepos',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControltypestyleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getControltypestyle()
        );
    }

    /**
     * @test
     */
    public function setControltypestyleForStringSetsControltypestyle()
    {
        $this->subject->setControltypestyle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'controltypestyle',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlstreetviewReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getControlstreetview()
        );
    }

    /**
     * @test
     */
    public function setControlstreetviewForBoolSetsControlstreetview()
    {
        $this->subject->setControlstreetview(true);

        self::assertAttributeEquals(
            true,
            'controlstreetview',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlstreetviewposReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getControlstreetviewpos()
        );
    }

    /**
     * @test
     */
    public function setControlstreetviewposForStringSetsControlstreetviewpos()
    {
        $this->subject->setControlstreetviewpos('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'controlstreetviewpos',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlrotateReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getControlrotate()
        );
    }

    /**
     * @test
     */
    public function setControlrotateForBoolSetsControlrotate()
    {
        $this->subject->setControlrotate(true);

        self::assertAttributeEquals(
            true,
            'controlrotate',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlrotateposReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getControlrotatepos()
        );
    }

    /**
     * @test
     */
    public function setControlrotateposForStringSetsControlrotatepos()
    {
        $this->subject->setControlrotatepos('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'controlrotatepos',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlscaleReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getControlscale()
        );
    }

    /**
     * @test
     */
    public function setControlscaleForBoolSetsControlscale()
    {
        $this->subject->setControlscale(true);

        self::assertAttributeEquals(
            true,
            'controlscale',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlfullscreenReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getControlfullscreen()
        );
    }

    /**
     * @test
     */
    public function setControlfullscreenForBoolSetsControlfullscreen()
    {
        $this->subject->setControlfullscreen(true);

        self::assertAttributeEquals(
            true,
            'controlfullscreen',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getControlfullscreenposReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getControlfullscreenpos()
        );
    }

    /**
     * @test
     */
    public function setControlfullscreenposForStringSetsControlfullscreenpos()
    {
        $this->subject->setControlfullscreenpos('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'controlfullscreenpos',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getPanningReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getPanning()
        );
    }

    /**
     * @test
     */
    public function setPanningForBoolSetsPanning()
    {
        $this->subject->setPanning(true);

        self::assertAttributeEquals(
            true,
            'panning',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getKeyboardshortcutsReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getKeyboardshortcuts()
        );
    }

    /**
     * @test
     */
    public function setKeyboardshortcutsForBoolSetsKeyboardshortcuts()
    {
        $this->subject->setKeyboardshortcuts(true);

        self::assertAttributeEquals(
            true,
            'keyboardshortcuts',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getDoubleclickzoomReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getDoubleclickzoom()
        );
    }

    /**
     * @test
     */
    public function setDoubleclickzoomForBoolSetsDoubleclickzoom()
    {
        $this->subject->setDoubleclickzoom(true);

        self::assertAttributeEquals(
            true,
            'doubleclickzoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getScrollzoomReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getScrollzoom()
        );
    }

    /**
     * @test
     */
    public function setScrollzoomForBoolSetsScrollzoom()
    {
        $this->subject->setScrollzoom(true);

        self::assertAttributeEquals(
            true,
            'scrollzoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getClickablepointsReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getClickablepoints()
        );
    }

    /**
     * @test
     */
    public function setClickablepointsForBoolSetsClickablepoints()
    {
        $this->subject->setClickablepoints(true);

        self::assertAttributeEquals(
            true,
            'clickablepoints',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMcReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMc()
        );
    }

    /**
     * @test
     */
    public function setMcForBoolSetsMc()
    {
        $this->subject->setMc(true);

        self::assertAttributeEquals(
            true,
            'mc',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMctypeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMctype()
        );
    }

    /**
     * @test
     */
    public function setMctypeForIntSetsMctype()
    {
        $this->subject->setMctype(12);

        self::assertAttributeEquals(
            12,
            'mctype',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMcgridsizeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMcgridsize()
        );
    }

    /**
     * @test
     */
    public function setMcgridsizeForIntSetsMcgridsize()
    {
        $this->subject->setMcgridsize(12);

        self::assertAttributeEquals(
            12,
            'mcgridsize',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMcmaxzoomReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMcmaxzoom()
        );
    }

    /**
     * @test
     */
    public function setMcmaxzoomForIntSetsMcmaxzoom()
    {
        $this->subject->setMcmaxzoom(12);

        self::assertAttributeEquals(
            12,
            'mcmaxzoom',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMczoomonclickReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMczoomonclick()
        );
    }

    /**
     * @test
     */
    public function setMczoomonclickForBoolSetsMczoomonclick()
    {
        $this->subject->setMczoomonclick(true);

        self::assertAttributeEquals(
            true,
            'mczoomonclick',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMcaveragecenterReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMcaveragecenter()
        );
    }

    /**
     * @test
     */
    public function setMcaveragecenterForBoolSetsMcaveragecenter()
    {
        $this->subject->setMcaveragecenter(true);

        self::assertAttributeEquals(
            true,
            'mcaveragecenter',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMcminclustersizeReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMcminclustersize()
        );
    }

    /**
     * @test
     */
    public function setMcminclustersizeForIntSetsMcminclustersize()
    {
        $this->subject->setMcminclustersize(12);

        self::assertAttributeEquals(
            12,
            'mcminclustersize',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMcstylesReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMcstyles()
        );
    }

    /**
     * @test
     */
    public function setMcstylesForStringSetsMcstyles()
    {
        $this->subject->setMcstyles('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mcstyles',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getItemReturnsInitialValueForItem()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getItem()
        );
    }

    /**
     * @test
     */
    public function setItemForObjectStorageContainingItemSetsItem()
    {
        $item = new \KITT3N\Kitt3nMap\Domain\Model\Item();
        $objectStorageHoldingExactlyOneItem = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneItem->attach($item);
        $this->subject->setItem($objectStorageHoldingExactlyOneItem);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneItem,
            'item',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addItemToObjectStorageHoldingItem()
    {
        $item = new \KITT3N\Kitt3nMap\Domain\Model\Item();
        $itemObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $itemObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($item));
        $this->inject($this->subject, 'item', $itemObjectStorageMock);

        $this->subject->addItem($item);
    }

    /**
     * @test
     */
    public function removeItemFromObjectStorageHoldingItem()
    {
        $item = new \KITT3N\Kitt3nMap\Domain\Model\Item();
        $itemObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $itemObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($item));
        $this->inject($this->subject, 'item', $itemObjectStorageMock);

        $this->subject->removeItem($item);
    }
}
