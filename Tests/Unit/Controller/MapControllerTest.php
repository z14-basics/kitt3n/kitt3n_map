<?php
namespace KITT3N\Kitt3nMap\Tests\Unit\Controller;

/**
 * Test case.
 *
 * @author Oliver Merz <o.merz@kitt3n.de>
 * @author Georg Kathan <g.kathan@kitt3n.de>
 * @author Dominik Hilser <d.hilser@kitt3n.de>
 */
class MapControllerTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \KITT3N\Kitt3nMap\Controller\MapController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\KITT3N\Kitt3nMap\Controller\MapController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
