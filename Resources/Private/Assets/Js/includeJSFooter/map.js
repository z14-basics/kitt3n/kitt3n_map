var kitt3n_map__interval = setInterval(function () {
    if ( typeof kitt3n_windowLoad == 'undefined') {
    } else {

        if (typeof kitt3n_windowLoad == "boolean" && kitt3n_windowLoad) {

            if (kitt3n_applicationContext == "Development") {
                console.info('kitt3n map :: map.js script is loaded!');
            }

            clearInterval(kitt3n_map__interval);

            var aMaps = [];
            var oMaps = document.getElementsByClassName('kitt3n_map--map');

            // done: find and loop all existing maps
            if (typeof oMaps == "object" && oMaps.length > 0) {
                for (var i = 0; i < oMaps.length; i++) {
                    (function (i) {

                        aMaps[i] = oMaps[i];

                        // done: init each map
                        initKitt3nMap(oMaps[i]);
                    })(i);
                }
            }
        }
    }

}, 1000);

// done: declare global variables
var bFitBounds;
var bMarkerClusterer;
var iMarkerClustererType;

// todo: implement
// var bounds = new google.maps.LatLngBounds();


var bounds;
var cmarkers;


// todo: init function
var aMaps = [];
var k = 0;
function initKitt3nMap(oMapDOM) {

    bFitBounds = parseInt(oMapDOM.getAttribute("data-km-fitbounds"));
    bMarkerClusterer = parseInt(oMapDOM.getAttribute("data-km-markercluster"));
    iMarkerClustererType = parseInt(oMapDOM.getAttribute("data-km-markercluster-type"));
    cmarkers = [];
    var infowindow;

    var loaded_json;
    var sMapStylesPath = oMapDOM.getAttribute("data-km-styles");


    loadJSON(function(response) {
        // done: styles *.json - loaded successfully
        if(response){
            loaded_json = JSON.parse(response);

            var sMapStyle = new google.maps.StyledMapType(
                loaded_json,
                {
                    name: "Map"
                }
            );
        }

        // done: get options of the current map
        var sOptions = oMapDOM.getAttribute("data-km-options");

        // done: parse json data from html data attribute
        var sFormattedMapOptions;
        try {
            sFormattedMapOptions = JSON.parse(sOptions);
        } catch (e) {
            sFormattedMapOptions = JSON.parse(JSON.stringify(sOptions));
        }

        // done: reset control options correctly
        if("zoomControlOptions" in sFormattedMapOptions){
            sFormattedMapOptions.zoomControlOptions.position = setMapOptionPosition(sFormattedMapOptions.zoomControlOptions.position);
        }
        if("mapTypeControlOptions" in sFormattedMapOptions){
            sFormattedMapOptions.mapTypeControlOptions.position = setMapOptionPosition(sFormattedMapOptions.mapTypeControlOptions.position);
        }
        if("mapTypeControlOptions" in sFormattedMapOptions){
            sFormattedMapOptions.mapTypeControlOptions.style = setMapTypeControlStyle(sFormattedMapOptions.mapTypeControlOptions.style);
        }
        if("streetViewControlOptions" in sFormattedMapOptions){
            sFormattedMapOptions.streetViewControlOptions.position = setMapOptionPosition(sFormattedMapOptions.streetViewControlOptions.position);
        }
        if("rotateControlOptions" in sFormattedMapOptions){
            sFormattedMapOptions.rotateControlOptions.position = setMapOptionPosition(sFormattedMapOptions.rotateControlOptions.position);
        }
        if("fullscreenControlOptions" in sFormattedMapOptions){
            sFormattedMapOptions.fullscreenControlOptions.position = setMapOptionPosition(sFormattedMapOptions.fullscreenControlOptions.position);
        }

        // done: init map
        var map = new google.maps.Map(oMapDOM, sFormattedMapOptions);

        // done: set map styles if "sMapStyle" is set
        if(sMapStyle){
            map.mapTypes.set("styled_map", sMapStyle);
            map.setMapTypeId("styled_map");
        }

        // done: init bounds
        if(bFitBounds){
            bounds = new google.maps.LatLngBounds();
        }

        // done: get markers from data attribute
        var sMarkers = oMapDOM.getAttribute("data-km-markers");
        var oMarkers;
        try {
            oMarkers = JSON.parse(sMarkers);
        } catch (e) {
            oMarkers = JSON.parse(JSON.stringify(sMarkers));
        }

        // done: loop markers
        if(typeof oMarkers != "undefined" && oMarkers instanceof Array){
            if(oMarkers.length > 0){
                for (var i=0; i < oMarkers.length; i++){
                    // todo: get / assign condition values (animation delay for each marker)
                    if(oMarkers[i]["animation"]["delay"] > 0 && isInteger(oMarkers[i]["animation"]["delay"])){
                        // animation with timeout
                        setTimeout(function(i) {
                            cmarkers.push(
                                // done: implement addKitt3nMarker() with all its options
                                addKitt3nMarker(map, i, oMarkers[i]["type"], oMarkers[i]["lat"], oMarkers[i]["lon"], oMarkers[i]["path"], oMarkers[i]["icon"]["width"], oMarkers[i]["icon"]["height"], oMarkers[i]["icon"]["scale"], oMarkers[i]["icon"]["anchorX"], oMarkers[i]["icon"]["anchorY"], oMarkers[i]["icon"]["fillcolor"], oMarkers[i]["icon"]["fillopacity"], oMarkers[i]["icon"]["strokecolor"], oMarkers[i]["icon"]["strokeopacity"], oMarkers[i]["icon"]["strokewidth"], oMarkers[i]["animation"]["type"], oMarkers[i]["window"]["text"], oMarkers[i]["window"]["image"]["src"], oMarkers[i]["window"]["image"]["description"], oMarkers[i]["window"]["state"])
                            );
                        }, oMarkers[i]["animation"]["delay"], i, markers, map, cmarkers);
                    } else {
                        // no animation
                        cmarkers.push(
                            // done: implement addKitt3nMarker() with all its options
                            addKitt3nMarker(map, i, oMarkers[i]["type"], oMarkers[i]["lat"], oMarkers[i]["lon"], oMarkers[i]["path"], oMarkers[i]["icon"]["width"], oMarkers[i]["icon"]["height"], oMarkers[i]["icon"]["scale"], oMarkers[i]["icon"]["anchorX"], oMarkers[i]["icon"]["anchorY"], oMarkers[i]["icon"]["fillcolor"], oMarkers[i]["icon"]["fillopacity"], oMarkers[i]["icon"]["strokecolor"], oMarkers[i]["icon"]["strokeopacity"], oMarkers[i]["icon"]["strokewidth"], oMarkers[i]["animation"]["type"], oMarkers[i]["window"]["text"], oMarkers[i]["window"]["image"]["src"], oMarkers[i]["window"]["image"]["description"], oMarkers[i]["window"]["state"])
                        );
                    }
                }
            }
        }

        // done: get markers from data attribute
        var sPolygons = oMapDOM.getAttribute("data-km-polygons");
        var oPolygons;
        try {
            oPolygons = JSON.parse(sPolygons);
        } catch (e) {
            oPolygons = JSON.parse(JSON.stringify(sPolygons));
        }

        // done: loop polygons
        if(typeof oPolygons != "undefined" && oPolygons instanceof Array){
            if(oPolygons.length > 0){
                for (var j=0; j < oPolygons.length; j++){
                    // done: implement addKitt3nPolygon() with all its options
                    addKitt3nPolygon(map, j, oPolygons[j]["paths"], oPolygons[j]["strokeColor"], oPolygons[j]["strokeOpacity"], oPolygons[j]["strokeWeight"], oPolygons[j]["fillColor"], oPolygons[j]["fillOpacity"]);
                }
            }
        }

        // done: execute bounds
        if(bFitBounds){
            map.fitBounds(bounds);
        }

        // done: init + execute marker clusters
        if (bMarkerClusterer){

            // done: get marker cluster options from data attribute
            var sMarkerClusterOptions = oMapDOM.getAttribute("data-km-markercluster-options");
            var oMarkerClusterOptions;
            try {
                oMarkerClusterOptions = JSON.parse(sMarkerClusterOptions);
            } catch (e) {
                oMarkerClusterOptions = JSON.parse(JSON.stringify(sMarkerClusterOptions));
            }

            // done: init cluster todo: load cluster resource once if a map on the site has enabled that feature
            var markerCluster = new MarkerClusterer(map, cmarkers, oMarkerClusterOptions);
        }

        k++;

        // done: load styles source path dynamically
    }, sMapStylesPath);
}

// done: add marker with specific options to map
function addKitt3nMarker(map, id, type, lat, lon, icoPath, icoWidth, icoHeight, icoScale, icoAnchorX, icoAnchorY, icoFillcolor, icoFillopacity, icoStrokecolor, icoStrokeopacity, icoStrokeWidth, animationType, infoWindowText, infoWindowImageSrc, infoWindowImageDesc, infoWindowState){
    // get position from coordinates
    var position = new google.maps.LatLng(lat, lon);

    // done: init marker options
    switch (type) {
        case 1:
            var customizableImage = {
                path: icoPath,
                fillColor: icoFillcolor,
                fillOpacity: icoFillopacity,
                scale: icoScale,
                strokeColor: icoStrokecolor,
                strokeWeight: icoStrokeWidth,

                // done: important to use size (values) of original SVG and not of the scaled one
                anchor: new google.maps.Point(16, 32)
            };
            var aMarkerOptions = {
                id: id,
                position: position,
                map: map,
                optimized: false,
                icon: customizableImage,
            };
            break;
        case 2:
            var customImage = {
                url: icoPath,
                size: new google.maps.Size(icoWidth, icoHeight),
                scaledSize: new google.maps.Size(icoWidth, icoHeight),

                origin: new google.maps.Point(0, 0),
                anchor: new google.maps.Point(icoAnchorX, icoAnchorY),
            };
            var aMarkerOptions = {
                id: id,
                position: position,
                map: map,
                optimized: false,
                icon: customImage,
            };
            break;
        default:
            var aMarkerOptions = {
                id: id,
                position: position,
                map: map,
            };
            break;
    }

    // done: init marker animation
    if(animationType != "") {
        switch(animationType){
            case "DROP":
                aMarkerOptions["animation"] = google.maps.Animation.DROP;
                break;
            case "BOUNCE":
                aMarkerOptions["animation"] = google.maps.Animation.BOUNCE;
                break;
            default:
                aMarkerOptions["animation"] = "";
                break;
        }
    }

    // done: init info window
    if(infoWindowText){
        var sInfoWindowContent;
        if (infoWindowImageSrc){
            sInfoWindowContent = '<div class="tableContainer">' + '<div class="tableCell">' + infoWindowText + '</div>' + '<div class="tableCell">' + infoWindowImageSrc + (infoWindowImageDesc ? '<br/>' + infoWindowImageDesc : '') + '</div>' + '</div>';
        } else {
            sInfoWindowContent = infoWindowText;
        }

        infowindow = new google.maps.InfoWindow();
        infowindow.setContent(sInfoWindowContent);
        // var infowindow = new google.maps.InfoWindow({
        //     content: sInfoWindowContent,
        // });
        // aMarkerOptions["infowindow"] = infowindow;
        // console.log(map)
    }

    // done: init marker
    var oMarker = new google.maps.Marker(aMarkerOptions);
    oMarker.set("id", id);

    // done: behavior info window (events) fixme: close all infowindows when click on marker and open that one
    if(infoWindowText){
        oMarker.addListener("click", function() {
            // hideAllInfoWindows(map);
            // console.log('clicked...');

            //Close active window if exists
            // if(activeWindow != null) activeWindow.close();

            //Open new window
            infowindow.open(map, oMarker);

            //Store new window in global variable
            // activeWindow = infowindow;
        });

        map.addListener("click", function() {
            infowindow.close();
        });

        if (infoWindowState){
            infowindow.open(map, oMarker);
        }
    }

    // done: add markers to bounds object
    if(bFitBounds){
        bounds.extend(new google.maps.LatLng(lat, lon));
    }

    // return marker
    return oMarker;
}

// done: add a polygon (area) to a map
// todo: Documentation the following sources
// https://gis.stackexchange.com/questions/183248/getting-polygon-boundaries-of-city-in-json-from-google-maps-api
// https://nominatim.openstreetmap.org/
// http://polygons.openstreetmap.fr/index.py
// http://www.birdtheme.org/useful/v3tool.html
function addKitt3nPolygon(map, id, url, strokeColor, strokeOpacity, strokeWeight, fillColor, fillOpacity){
    var pathX = [];
    var xhr = new XMLHttpRequest();
    xhr.open("GET", url, true);
    xhr.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            var resultArray = JSON.parse(this.responseText);

            for (var i=0; i<resultArray.length; i++) {
                pathX[i] = {
                    lat: resultArray[i].lat,
                    lng: resultArray[i].lng
                };


                if(bFitBounds){
                    // Add polygon coordinates to bounds object
                    bounds.extend(new google.maps.LatLng(pathX[i]["lat"], pathX[i]["lng"]));
                }
            }

            var oPolygon = new google.maps.Polygon({
                paths: pathX,
                strokeColor: strokeColor,
                strokeOpacity: strokeOpacity,
                strokeWeight: strokeWeight,
                fillColor: fillColor,
                fillOpacity: fillOpacity
            });
            oPolygon.setMap(map);

            return oPolygon;
        }
    };
    xhr.send();

}

// todo: hide all info windows of a map !!! cmarkers
function hideAllInfoWindows(map) {
    // console.log(cmarkers);
    cmarkers.forEach(function(oMarker) {
        oMarker.infowindow.close(map, oMarker);
    });
}

// done: set map option position to valid value
function setMapOptionPosition($controlPosition){
    switch($controlPosition){
        case "BOTTOM_CENTER":
            return google.maps.ControlPosition.BOTTOM_CENTER;
            break;
        case "BOTTOM_LEFT":
            return google.maps.ControlPosition.BOTTOM_LEFT;
            break;
        case "BOTTOM_RIGHT":
            return google.maps.ControlPosition.BOTTOM_RIGHT;
            break;
        case "LEFT_BOTTOM":
            return google.maps.ControlPosition.LEFT_BOTTOM;
            break;
        case "LEFT_CENTER":
            return google.maps.ControlPosition.LEFT_CENTER;
            break;
        case "LEFT_TOP":
            return google.maps.ControlPosition.LEFT_TOP;
            break;
        case "RIGHT_BOTTOM":
            return google.maps.ControlPosition.RIGHT_BOTTOM;
            break;
        case "RIGHT_CENTER":
            return google.maps.ControlPosition.RIGHT_CENTER;
            break;
        case "RIGHT_TOP":
            return google.maps.ControlPosition.RIGHT_TOP;
            break;
        case "TOP_CENTER":
            return google.maps.ControlPosition.TOP_CENTER;
            break;
        case "TOP_LEFT":
            return google.maps.ControlPosition.TOP_LEFT;
            break;
        default:
            return google.maps.ControlPosition.TOP_RIGHT;
            break;
    }
}

// done: set map option type-control-style to valid value
function setMapTypeControlStyle($controlStyle){
    switch($controlStyle){
        case "HORIZONTAL_BAR":
            return google.maps.MapTypeControlStyle.HORIZONTAL_BAR;
            break;
        case "DROPDOWN_MENU":
            return google.maps.MapTypeControlStyle.DROPDOWN_MENU;
            break;
        default:
            return google.maps.MapTypeControlStyle.DEFAULT;
            break;
    }
}

// done: load json file
function loadJSON(callback, url) {
    if(url){
        var xobj = new XMLHttpRequest();
        xobj.overrideMimeType("application/json");
        xobj.open("GET", url, true);
        xobj.onreadystatechange = function() {
            if (xobj.readyState == 4 && xobj.status == "200") {
                callback(xobj.responseText);
            }
        };
        xobj.send(null);
    } else {
        callback();
    }
}

// done: check if value is integer
function isInteger(x) {
    return x % 1 === 0;
}