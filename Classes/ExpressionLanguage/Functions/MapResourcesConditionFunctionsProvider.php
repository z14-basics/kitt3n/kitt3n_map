<?php
declare(strict_types = 1);

namespace KITT3N\Kitt3nMap\ExpressionLanguage\Functions;

/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

use Symfony\Component\ExpressionLanguage\ExpressionFunction;
use Symfony\Component\ExpressionLanguage\ExpressionFunctionProviderInterface;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Utility\GeneralUtility;

// todo: make it work...
class MapResourcesConditionFunctionsProvider implements ExpressionFunctionProviderInterface
{
    const CTYPE = "kitt3nmap_kitt3nmaprender";

    /**
     * @return ExpressionFunction[] An array of Function instances
     */
    public function getFunctions()
    {
        return [
            $this->getMapResources(),
            $this->getMapClusterResources()
        ];
    }

    /**
     * Shortcut function to access field values
     *
     * @return \Symfony\Component\ExpressionLanguage\ExpressionFunction
     */
    protected function getMapResources(): ExpressionFunction
    {
        return new ExpressionFunction('mapResources', function () {
            // Not implemented, we only use the evaluator
        }, function ($arguments) {

            //...
            $bMapResourceState = FALSE;
            $iCurrentPid = $GLOBALS["TSFE"]->id;

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tt_content')->createQueryBuilder();

            $rows = $queryBuilder
                ->select('uid', 'pi_flexform')
                ->from('tt_content')
                ->where(
                    $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($iCurrentPid, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('list_type', $queryBuilder->createNamedParameter(self::CTYPE)),
                    $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('hidden', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
                )
                ->execute()->fetchAll();

            if (count($rows) > 0){
                $bMapResourceState = TRUE;
            }

            return $bMapResourceState;

        });
    }

    /**
     * Shortcut function to access field values
     *
     * @return \Symfony\Component\ExpressionLanguage\ExpressionFunction
     */
    protected function getMapClusterResources(): ExpressionFunction
    {
        return new ExpressionFunction('mapClusterResources', function () {
            // Not implemented, we only use the evaluator
        }, function ($arguments) {

            //...
            $bMapClusterResourceState = FALSE;
            $iCurrentPid = $GLOBALS["TSFE"]->id;

            $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tt_content')->createQueryBuilder();

            $rows = $queryBuilder
                ->select('uid', 'pi_flexform')
                ->from('tt_content')
                ->where(
                    $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($iCurrentPid, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('list_type', $queryBuilder->createNamedParameter(self::CTYPE)),
                    $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                    $queryBuilder->expr()->eq('hidden', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
                )
                ->execute()->fetchAll();

            if (count($rows) > 0){

                foreach ($rows as $row){
                    $xmlfile = $row['pi_flexform'];
                    $ob = simplexml_load_string($xmlfile);
                    $json = json_encode($ob);
                    $configData = json_decode($json, true);
                    $iMapUid = intval($configData['data']['sheet']['language']['field']['value']);

                    $mcQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_kitt3nmap_domain_model_map')->createQueryBuilder();

                    $mcRows = $mcQueryBuilder
                        ->select('uid', 'mc')
                        ->from('tx_kitt3nmap_domain_model_map')
                        ->where(
                            $mcQueryBuilder->expr()->eq('uid', $mcQueryBuilder->createNamedParameter($iMapUid, \PDO::PARAM_INT)),
                            $mcQueryBuilder->expr()->eq('mc', $mcQueryBuilder->createNamedParameter(1, \PDO::PARAM_INT)),
                            $mcQueryBuilder->expr()->eq('deleted', $mcQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                            $mcQueryBuilder->expr()->eq('hidden', $mcQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
                        )
                        ->execute()->fetchAll();

                    if (count($mcRows) > 0){
                        $bMapClusterResourceState = TRUE;
                    }

                }
            }

            return $bMapClusterResourceState;

        });
    }
}
