<?php
namespace KITT3N\Kitt3nMap\Domain\Model;


/***
 *
 * This file is part of the "kitt3n | Map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * Item
 */
class Item extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * type
     * 
     * @var int
     */
    protected $type = 0;

    /**
     * name
     * 
     * @var string
     */
    protected $name = '';

    /**
     * lat
     * 
     * @var string
     */
    protected $lat = '';

    /**
     * lon
     * 
     * @var string
     */
    protected $lon = '';

    /**
     * icon
     * 
     * @var string
     */
    protected $icon = '';

    /**
     * icondefaults
     * 
     * @var int
     */
    protected $icondefaults = 0;

    /**
     * iconcolor
     * 
     * @var string
     */
    protected $iconcolor = '';

    /**
     * iconwidth
     * 
     * @var int
     */
    protected $iconwidth = 0;

    /**
     * iconheight
     * 
     * @var int
     */
    protected $iconheight = 0;

    /**
     * iconoriginx
     * 
     * @var int
     */
    protected $iconoriginx = 0;

    /**
     * iconoriginy
     * 
     * @var int
     */
    protected $iconoriginy = 0;

    /**
     * iconfile
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $iconfile = null;

    /**
     * paths
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     * @validate NotEmpty
     */
    protected $paths = '';

    /**
     * strokecolor
     * 
     * @var string
     */
    protected $strokecolor = '';

    /**
     * strokeopacity
     * 
     * @var string
     */
    protected $strokeopacity = '';

    /**
     * strokewidth
     * 
     * @var int
     */
    protected $strokewidth = '';

    /**
     * fillcolor
     * 
     * @var string
     */
    protected $fillcolor = '';

    /**
     * fillopacity
     * 
     * @var string
     */
    protected $fillopacity = '';

    /**
     * scale
     * 
     * @var float
     */
    protected $scale = 0.0;

    /**
     * window
     * 
     * @var string
     */
    protected $window = '';

    /**
     * windowimage
     * 
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $windowimage = null;

    /**
     * windowstate
     * 
     * @var bool
     */
    protected $windowstate = false;

    /**
     * animationtype
     * 
     * @var string
     */
    protected $animationtype = '';

    /**
     * animationdelay
     * 
     * @var int
     */
    protected $animationdelay = 0;

    /**
     * Returns the type
     * 
     * @return int $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     * 
     * @param int $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Returns the lat
     * 
     * @return string $lat
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * Sets the lat
     * 
     * @param string $lat
     * @return void
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * Returns the lon
     * 
     * @return string $lon
     */
    public function getLon()
    {
        return $this->lon;
    }

    /**
     * Sets the lon
     * 
     * @param string $lon
     * @return void
     */
    public function setLon($lon)
    {
        $this->lon = $lon;
    }

    /**
     * Returns the icondefaults
     * 
     * @return int $icondefaults
     */
    public function getIcondefaults()
    {
        return $this->icondefaults;
    }

    /**
     * Sets the icondefaults
     * 
     * @param int $icondefaults
     * @return void
     */
    public function setIcondefaults($icondefaults)
    {
        $this->icondefaults = $icondefaults;
    }

    /**
     * Returns the iconcolor
     * 
     * @return string $iconcolor
     */
    public function getIconcolor()
    {
        return $this->iconcolor;
    }

    /**
     * Sets the iconcolor
     * 
     * @param string $iconcolor
     * @return void
     */
    public function setIconcolor($iconcolor)
    {
        $this->iconcolor = $iconcolor;
    }

    /**
     * Returns the iconfile
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $iconfile
     */
    public function getIconfile()
    {
        return $this->iconfile;
    }

    /**
     * Sets the iconfile
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $iconfile
     * @return void
     */
    public function setIconfile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $iconfile)
    {
        $this->iconfile = $iconfile;
    }

    /**
     * Returns the strokecolor
     * 
     * @return string $strokecolor
     */
    public function getStrokecolor()
    {
        return $this->strokecolor;
    }

    /**
     * Sets the strokecolor
     * 
     * @param string $strokecolor
     * @return void
     */
    public function setStrokecolor($strokecolor)
    {
        $this->strokecolor = $strokecolor;
    }

    /**
     * Returns the strokeopacity
     * 
     * @return string $strokeopacity
     */
    public function getStrokeopacity()
    {
        return $this->strokeopacity;
    }

    /**
     * Sets the strokeopacity
     * 
     * @param string $strokeopacity
     * @return void
     */
    public function setStrokeopacity($strokeopacity)
    {
        $this->strokeopacity = $strokeopacity;
    }

    /**
     * Returns the fillcolor
     * 
     * @return string $fillcolor
     */
    public function getFillcolor()
    {
        return $this->fillcolor;
    }

    /**
     * Sets the fillcolor
     * 
     * @param string $fillcolor
     * @return void
     */
    public function setFillcolor($fillcolor)
    {
        $this->fillcolor = $fillcolor;
    }

    /**
     * Returns the fillopacity
     * 
     * @return string $fillopacity
     */
    public function getFillopacity()
    {
        return $this->fillopacity;
    }

    /**
     * Sets the fillopacity
     * 
     * @param string $fillopacity
     * @return void
     */
    public function setFillopacity($fillopacity)
    {
        $this->fillopacity = $fillopacity;
    }

    /**
     * Returns the icon
     * 
     * @return string $icon
     */
    public function getIcon()
    {
        return $this->icon;
    }

    /**
     * Sets the icon
     * 
     * @param string $icon
     * @return void
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;
    }

    /**
     * Returns the iconoriginx
     * 
     * @return int $iconoriginx
     */
    public function getIconoriginx()
    {
        return $this->iconoriginx;
    }

    /**
     * Sets the iconoriginx
     * 
     * @param int $iconoriginx
     * @return void
     */
    public function setIconoriginx($iconoriginx)
    {
        $this->iconoriginx = $iconoriginx;
    }

    /**
     * Returns the iconoriginy
     * 
     * @return int $iconoriginy
     */
    public function getIconoriginy()
    {
        return $this->iconoriginy;
    }

    /**
     * Sets the iconoriginy
     * 
     * @param int $iconoriginy
     * @return void
     */
    public function setIconoriginy($iconoriginy)
    {
        $this->iconoriginy = $iconoriginy;
    }

    /**
     * Returns the iconwidth
     * 
     * @return int iconwidth
     */
    public function getIconwidth()
    {
        return $this->iconwidth;
    }

    /**
     * Sets the iconwidth
     * 
     * @param int $iconwidth
     * @return void
     */
    public function setIconwidth($iconwidth)
    {
        $this->iconwidth = $iconwidth;
    }

    /**
     * Returns the iconheight
     * 
     * @return int $iconheight
     */
    public function getIconheight()
    {
        return $this->iconheight;
    }

    /**
     * Sets the iconheight
     * 
     * @param int $iconheight
     * @return void
     */
    public function setIconheight($iconheight)
    {
        $this->iconheight = $iconheight;
    }

    /**
     * Returns the scale
     * 
     * @return float $scale
     */
    public function getScale()
    {
        return $this->scale;
    }

    /**
     * Sets the scale
     * 
     * @param float $scale
     * @return void
     */
    public function setScale($scale)
    {
        $this->scale = $scale;
    }

    /**
     * Returns the strokewidth
     * 
     * @return int strokewidth
     */
    public function getStrokewidth()
    {
        return $this->strokewidth;
    }

    /**
     * Sets the strokewidth
     * 
     * @param string $strokewidth
     * @return void
     */
    public function setStrokewidth($strokewidth)
    {
        $this->strokewidth = $strokewidth;
    }

    /**
     * Returns the window
     * 
     * @return string $window
     */
    public function getWindow()
    {
        return $this->window;
    }

    /**
     * Sets the window
     * 
     * @param string $window
     * @return void
     */
    public function setWindow($window)
    {
        $this->window = $window;
    }

    /**
     * Returns the paths
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference paths
     */
    public function getPaths()
    {
        return $this->paths;
    }

    /**
     * Sets the paths
     * 
     * @param string $paths
     * @return void
     */
    public function setPaths($paths)
    {
        $this->paths = $paths;
    }

    /**
     * Returns the windowimage
     * 
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $windowimage
     */
    public function getWindowimage()
    {
        return $this->windowimage;
    }

    /**
     * Sets the windowimage
     * 
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $windowimage
     * @return void
     */
    public function setWindowimage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $windowimage)
    {
        $this->windowimage = $windowimage;
    }

    /**
     * Returns the windowstate
     * 
     * @return bool $windowstate
     */
    public function getWindowstate()
    {
        return $this->windowstate;
    }

    /**
     * Sets the windowstate
     * 
     * @param bool $windowstate
     * @return void
     */
    public function setWindowstate($windowstate)
    {
        $this->windowstate = $windowstate;
    }

    /**
     * Returns the boolean state of windowstate
     * 
     * @return bool
     */
    public function isWindowstate()
    {
        return $this->windowstate;
    }

    /**
     * Returns the animationtype
     * 
     * @return string $animationtype
     */
    public function getAnimationtype()
    {
        return $this->animationtype;
    }

    /**
     * Sets the animationtype
     * 
     * @param string $animationtype
     * @return void
     */
    public function setAnimationtype($animationtype)
    {
        $this->animationtype = $animationtype;
    }

    /**
     * Returns the animationdelay
     * 
     * @return int $animationdelay
     */
    public function getAnimationdelay()
    {
        return $this->animationdelay;
    }

    /**
     * Sets the animationdelay
     * 
     * @param int $animationdelay
     * @return void
     */
    public function setAnimationdelay($animationdelay)
    {
        $this->animationdelay = $animationdelay;
    }
}
