<?php
namespace KITT3N\Kitt3nMap\UserFunc;

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Connection;


class MapResourcesUserFunc
{

    const CTYPE = "kitt3nmap_kitt3nmaprender";

    /**
     * check if map is used on page and include resource only if it's true
     *
     * @return boolean
     */
    public static function checkMapResource(){

        $iMapResourceState = FALSE;
        $iCurrentPid = $GLOBALS["TSFE"]->id;

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tt_content')->createQueryBuilder();

        $rows = $queryBuilder
            ->select('uid', 'pi_flexform')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($iCurrentPid, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('list_type', $queryBuilder->createNamedParameter(self::CTYPE)),
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('hidden', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute()->fetchAll();

        if (count($rows) > 0){
            $iMapResourceState = TRUE;
        }

        return $iMapResourceState;

    }



    /**
     *
     * check if any map on page uses marker clusters and include resource only if it's true
     *
     * @return integer
     */
    public static function checkMapClusterResource(){

        $iMapClusterResourceState = FALSE;
        $iCurrentPid = $GLOBALS["TSFE"]->id;

        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tt_content')->createQueryBuilder();

        $rows = $queryBuilder
            ->select('uid', 'pi_flexform')
            ->from('tt_content')
            ->where(
                $queryBuilder->expr()->eq('pid', $queryBuilder->createNamedParameter($iCurrentPid, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('list_type', $queryBuilder->createNamedParameter(self::CTYPE)),
                $queryBuilder->expr()->eq('deleted', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                $queryBuilder->expr()->eq('hidden', $queryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
            )
            ->execute()->fetchAll();

        if (count($rows) > 0){

            foreach ($rows as $row){
                $xmlfile = $row['pi_flexform'];
                $ob = simplexml_load_string($xmlfile);
                $json = json_encode($ob);
                $configData = json_decode($json, true);
                $iMapUid = intval($configData['data']['sheet']['language']['field']['value']);

                $mcQueryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getConnectionForTable('tx_kitt3nmap_domain_model_map')->createQueryBuilder();

                $mcRows = $mcQueryBuilder
                    ->select('uid', 'mc')
                    ->from('tx_kitt3nmap_domain_model_map')
                    ->where(
                        $mcQueryBuilder->expr()->eq('uid', $mcQueryBuilder->createNamedParameter($iMapUid, \PDO::PARAM_INT)),
                        $mcQueryBuilder->expr()->eq('mc', $mcQueryBuilder->createNamedParameter(1, \PDO::PARAM_INT)),
                        $mcQueryBuilder->expr()->eq('deleted', $mcQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT)),
                        $mcQueryBuilder->expr()->eq('hidden', $mcQueryBuilder->createNamedParameter(0, \PDO::PARAM_INT))
                    )
                    ->execute()->fetchAll();

                if (count($mcRows) > 0){
                    $iMapClusterResourceState = TRUE;
                }

            }
        }

        return $iMapClusterResourceState;

    }

}
