<?php
namespace KITT3N\Kitt3nMap\Controller;


/***
 *
 * This file is part of the "kitt3n | Map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019 
 *
 ***/
/**
 * ItemController
 */
class ItemController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    /**
     * itemRepository
     * 
     * @var \KITT3N\Kitt3nMap\Domain\Repository\ItemRepository
     * @inject
     */
    protected $itemRepository = null;

    /**
     * action list
     * 
     * @return void
     */
    public function listAction()
    {
        $items = $this->itemRepository->findAll();
        $this->view->assign('items', $items);
    }

    /**
     * action show
     * 
     * @param \KITT3N\Kitt3nMap\Domain\Model\Item $item
     * @return void
     */
    public function showAction(\KITT3N\Kitt3nMap\Domain\Model\Item $item)
    {
        $this->view->assign('item', $item);
    }
}
