<?php
namespace KITT3N\Kitt3nMap\Controller;

use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

/***
 *
 * This file is part of the "kitt3n | Map" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2019
 *
 ***/
/**
 * MapController
 */
class MapController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

    const COOKIE_NAME_KM_MAP = "bKitt3nMap";
    const COOKIE_NAME_KM_MARKERCLUSTER = "bKitt3nMapMarkerCluster";

    /**
     * mapRepository
     *
     * @var \KITT3N\Kitt3nMap\Domain\Repository\MapRepository
     * @inject
     */
    protected $mapRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager;


    /**
     * action render
     *
     * @return void
     */
    public function renderAction()
    {

        // todo: documentation...
        // ### get settings ###
        $aSettings = $this->settings;
        $oContentObj = $this->configurationManager->getContentObject();
        $iRecordUid = $oContentObj->data['uid'];

        // ### get selected kitt3n_map ###
        $sFlexformMapUid = $aSettings['sMap'];
        $oMap = $this->mapRepository->findByUid($sFlexformMapUid);

        // ### kitt3n_map configs ###
        // get styles *.json file
        $sMapStyleFilePath = FALSE;
        $oMapStyleFile = $oMap->getStylefile();
        $sMapStyleFile = $oMapStyleFile ? $this->validateJSONString($oMapStyleFile) : FALSE;
        if($sMapStyleFile){
            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                $sMapStyleFilePath = 'https://' . $_SERVER['HTTP_HOST'] . '/' . $oMapStyleFile->getOriginalResource()->getPublicUrl();
            } else {
                $sMapStyleFilePath = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $oMapStyleFile->getOriginalResource()->getPublicUrl();
            }
        }

        // get map settings
        $iMapWidth = $oMap->getWidth();
        $sMapWidthUnit = $oMap->getWidthunit() == 0 ? '%' : 'px';
        $iMapHeight = $oMap->getHeight();
        $sMapHeightUnit = $oMap->getHeightunit() == 0 ? '%' : 'px';
        $bMapFitBounds = $oMap->getFitbounds();
        $fMapLat = $oMap->getLat();
        $fMapLon = $oMap->getLon();
        $iMapZoom = $oMap->getZoom();
        $iMapMinZoom = $oMap->getMinzoom();
        $iMapMaxZoom = $oMap->getMaxzoom();
        $sMapLanguage = $oMap->getLanguage();
        $sMapType = strtolower($oMap->getType());

        // get controls settings
        $sMapControls = $oMap->getControls();
        $iMapControlSize = $oMap->getControlsize();
        $bMapControlZoom = $oMap->getControlzoom();
        $sMapControlZoomPos = $oMap->getControlzoompos();
        $bMapControlType = $oMap->getControltype();
        $sMapControlTypePos = $oMap->getControltypepos();
        $sMapControlTypeStyle = $oMap->getControltypestyle();
        $bMapControlStreetview = $oMap->getControlstreetview();
        $sMapControlStreetviewPos = $oMap->getControlstreetviewpos();
        $bMapControlRotate = $oMap->getControlrotate();
        $sMapControlRotatePos = $oMap->getControlrotatepos();
        $bMapControlScale = $oMap->getControlscale();
        $bMapControlFullscreen = $oMap->getControlfullscreen();
        $sMapControlFullscreenPos = $oMap->getControlfullscreenpos();

        // todo: implement tilt
        // get behavior settings
        $bMapPanning = $oMap->getPanning();
        $bMapKeyboardshortcuts = $oMap->getKeyboardshortcuts();
        $bMapDoubleclickzoom = $oMap->getDoubleclickzoom();
        $bMapScrollzoom = $oMap->getScrollzoom();
        $bMapClickablepoints = $oMap->getClickablepoints();

        // get marker cluster settings
        $bMapCluster = $oMap->getMc();
        $iMapClusterType = $oMap->getMctype();
        $iMapClusterGridsize = $oMap->getMcgridsize();
        $iMapClusterMaxzoom = $oMap->getMcmaxzoom();
        $bMapClusterZoomonclick = $oMap->getMczoomonclick();
        $bMapClusterAveragecenter = $oMap->getMcaveragecenter();
        $iMapClusterMinclustersize = $oMap->getMcminclustersize();
        $sMapClusterStyles = $oMap->getMcstyles();

        // get all related items (markers and polygons) for the map
        $oItems = $oMap->getItem();

        // ### generate marker array ###
        $aMarkers = [];
        $sMarkers = '';
        $aMarkerInfo = [];
        $i = 0;

        // loop items and set marker array with all it's configs
        foreach ($oItems as $oItem) {
            if ($oItem->getType() == 0) {
                $aMarkers[$i] = $oItem;
                $aMarkerInfo[$i]['type'] = 0;
                $aMarkerInfo[$i]['lat'] = $oItem->getLat();
                $aMarkerInfo[$i]['lon'] = $oItem->getLon();
                $aMarkerInfo[$i]['label'] = $oItem->getName();
                $aMarkerInfo[$i]['icon'] = [];
                $aMarkerInfo[$i]['icon']['width'] = ($oItem->getIconwidth() ? $oItem->getIconwidth() : 0);
                $aMarkerInfo[$i]['icon']['height'] = $oItem->getIconheight();
                $aMarkerInfo[$i]['icon']['scale'] = ($oItem->getScale() ? $oItem->getScale() : 0);
                $aMarkerInfo[$i]['icon']['anchorX'] = $oItem->getIconoriginx();
                $aMarkerInfo[$i]['icon']['anchorY'] = $oItem->getIconoriginy();
                $aMarkerInfo[$i]['icon']['fillcolor'] = $oItem->getFillcolor();
                $aMarkerInfo[$i]['icon']['fillopacity'] = ($oItem->getFillopacity() ? (double)$oItem->getFillopacity() : 0);
                $aMarkerInfo[$i]['icon']['strokecolor'] = $oItem->getStrokecolor();
                $aMarkerInfo[$i]['icon']['strokeopacity'] = ($oItem->getStrokeopacity() ? $oItem->getStrokeopacity() : 0);
                $aMarkerInfo[$i]['icon']['strokewidth'] = ($oItem->getStrokewidth() ? $oItem->getStrokewidth() : 0);

                $aMarkerInfo[$i]['animation']['type'] = ($oItem->getAnimationtype() != '' ? $oItem->getAnimationtype() : FALSE);
                $aMarkerInfo[$i]['animation']['delay'] = ($oItem->getAnimationdelay() ? $oItem->getAnimationdelay() : 0);

                $aMarkerInfo[$i]['window']['state'] = 0;
                $aMarkerInfo[$i]['window']['text'] = $this->formatRTE($oItem->getWindow());
                $aMarkerInfo[$i]['window']['image']['src'] = '';
                $aMarkerInfo[$i]['window']['image']['description'] = '';
                if ($oItem->getWindowstate()){
                    $aMarkerInfo[$i]['window']['state'] = $oItem->getWindowstate();
                }
                if ($oItem->getWindowimage()){
                    $aMarkerInfo[$i]['window']['image']['src'] = '<img src="/' . $oItem->getWindowimage()->getOriginalResource()->getPublicUrl() . '" width="' . $oItem->getWindowimage()->getOriginalResource()->getProperty('width') . '" height="' . $oItem->getWindowimage()->getOriginalResource()->getProperty('height') . '" alt="' . $oItem->getWindowimage()->getOriginalResource()->getAlternative() . '">';
                    $aMarkerInfo[$i]['window']['image']['description'] = $oItem->getWindowimage()->getOriginalResource()->getDescription();
                }


                // different settings for selected marker style (default, customizable, custom file)
                $iIconType = $oItem->getIcon();
                if ($iIconType) {
                    switch ($iIconType) {
                        case "1":
                            if ($oItem->getIcondefaults() != 0) {

                                switch ($oItem->getIcondefaults()) {
                                    case 2:

                                        // pin stroke
                                        $sMarkerIcon = 'M16.1 0C9.4 0 3.9 5.5 3.9 12.2c0 3.7 1.9 6.5 4 9.4L16.1 32l8.1-10.4c2.1-2.8 4-5.7 4-9.4C28.2 5.5 22.8.1 16.1 0zm6.6 20.4L16 28.9l-6.7-8.5c-2-2.7-3.6-5.2-3.6-8.3C5.8 6.5 10.4 1.9 16 1.9c5.7 0 10.3 4.6 10.3 10.3 0 3.1-1.6 5.5-3.6 8.2z';
                                        break;
                                    case 3:

                                        // pin stroke dot
                                        $sMarkerIcon = 'M16.1 0C9.4 0 3.9 5.5 3.9 12.2c0 3.7 1.9 6.5 4 9.4L16.1 32l8.1-10.4c2.1-2.8 4-5.7 4-9.4C28.2 5.5 22.8.1 16.1 0zm6.6 20.4L16 28.9l-6.7-8.5c-2-2.7-3.6-5.2-3.6-8.3C5.8 6.5 10.4 1.9 16 1.9c5.7 0 10.3 4.6 10.3 10.3 0 3.1-1.6 5.5-3.6 8.2z M10.600000000000001,12.1a5.5,5.5 0 1,0 11,0a5.5,5.5 0 1,0 -11,0';
                                        break;
                                    case 4:

                                        // pin filled with hole
                                        $sMarkerIcon = 'M16.1 0C9.4 0 3.9 5.5 3.9 12.2c0 3.7 1.9 6.5 4 9.4L16.1 32l8.1-10.4c2.1-2.8 4-5.7 4-9.4C28.2 5.5 22.8.1 16.1 0zm6.6 20.4L16 28.9l-6.7-8.5c-2-2.7-3.6-5.2-3.6-8.3C5.8 6.5 10.4 1.9 16 1.9c5.7 0 10.3 4.6 10.3 10.3 0 3.1-1.6 5.5-3.6 8.2z M23.7 3.8L16 .7l-7.5 3L4.8 10l.6 6.2 10.7 14L26 17.4l1.1-6.5-3.4-7.1zm-7.6 13.8c-3 0-5.5-2.5-5.5-5.5s2.5-5.5 5.5-5.5 5.5 2.5 5.5 5.5-2.5 5.5-5.5 5.5z';
                                        break;
                                    case 5:

                                        // pin stroke hole
                                        $sMarkerIcon = 'M16.1 0C9.4 0 3.9 5.5 3.9 12.2c0 3.7 1.9 6.5 4 9.4L16.1 32l8.1-10.4c2.1-2.8 4-5.7 4-9.4C28.2 5.5 22.8.1 16.1 0zm6.6 20.4L16 28.9l-6.7-8.5c-2-2.7-3.6-5.2-3.6-8.3C5.8 6.5 10.4 1.9 16 1.9c5.7 0 10.3 4.6 10.3 10.3 0 3.1-1.6 5.5-3.6 8.2z M16.1 4.8c-4.1 0-7.4 3.3-7.4 7.4 0 4.1 3.3 7.4 7.4 7.4 4.1 0 7.4-3.3 7.4-7.4-.1-4.1-3.4-7.4-7.4-7.4zm0 12.9c-3 0-5.5-2.5-5.5-5.5s2.5-5.5 5.5-5.5 5.5 2.5 5.5 5.5-2.5 5.5-5.5 5.5z';
                                        break;
                                    case 6:

                                        // shield filled
                                        $sMarkerIcon = 'M15.9 31.9l-12.2-6L.9.1h30.3L28 25.9l-12.1 6zM5.4 24.7l10.5 5.1 10.4-5.1L29.1 2H2.9l2.5 22.7z M1.7.9H30l-2.9 24.3-11.3 5.9-11.1-5.4z';
                                        break;
                                    case 7:

                                        // shield stroke
                                        $sMarkerIcon = 'M15.9 31.9l-12.2-6L.9.1h30.3L28 25.9l-12.1 6zM5.4 24.7l10.5 5.1 10.4-5.1L29.1 2H2.9l2.5 22.7z';
                                        break;
                                    case 8:

                                        // flag filled
                                        $sMarkerIcon = 'M16.1 32h-2V.1L32 8.6 16.1 16v16zm0-28.7v10.5l11.2-5.2-11.2-5.3z M15.2 2.3l.2 12.9 14-6.6z';
                                        break;
                                    case 9:

                                        // flag stroke
                                        $sMarkerIcon = 'M16.1 32h-2V.1L32 8.6 16.1 16v16zm0-28.7v10.5l11.2-5.2-11.2-5.3z';
                                        break;
                                    default:

                                        // pin filled
                                        $sMarkerIcon = 'M16.1 0C9.4 0 3.9 5.5 3.9 12.2c0 3.7 1.9 6.5 4 9.4L16.1 32l8.1-10.4c2.1-2.8 4-5.7 4-9.4C28.2 5.5 22.8.1 16.1 0zm6.6 20.4L16 28.9l-6.7-8.5c-2-2.7-3.6-5.2-3.6-8.3C5.8 6.5 10.4 1.9 16 1.9c5.7 0 10.3 4.6 10.3 10.3 0 3.1-1.6 5.5-3.6 8.2z M15.8 30.1l6.3-7.7 4.1-6.4.8-6.6-3-5.3-8-3.5S9.5 3.1 9.3 3.3c-.2.4-4 5.3-4 5.5 0 .2-.5 4.5-.4 4.8.1.3 2 5.6 2 5.6l8.9 10.9z';
                                        break;
                                }

                                // case 1 > customizable markers :: selected marker icon
                                $aMarkerInfo[$i]['type'] = 1;
                                $aMarkerInfo[$i]['path'] = $sMarkerIcon;
                            } else {

                                // case 1 > customizable markers :: no selection
                                $aMarkerInfo[$i]['path'] = '';
                                $aMarkerInfo[$i]['icon']['width'] = 32;
                                $aMarkerInfo[$i]['icon']['height'] = 32;
                            }
                            break;
                        case "2":
                            // document root
                            $sServerDocumentRoot = rtrim($_SERVER['DOCUMENT_ROOT'], '/') . '/';

                            // relative path of file
                            $sMarkerIcon = $oItem->getIconfile()->getOriginalResource()->getPublicUrl();

                            // absolute path of file
                            if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                                $sCustomMarkerPath = 'https://' . $_SERVER['HTTP_HOST'] . '/' . $sMarkerIcon;
                            } else {
                                $sCustomMarkerPath = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $sMarkerIcon;
                            }

                            // get file extension
                            $aFileInfo = pathinfo($sCustomMarkerPath);
                            $aMarkerInfo[$i]['icon']['file'] = $aFileInfo['extension'];
                            if ($aMarkerInfo[$i]['icon']['file'] == "png") {
                                $aFileDimensions = getimagesize($sCustomMarkerPath);
                                $aMarkerInfo[$i]['icon']['width'] = $aFileDimensions[0];
                                $aMarkerInfo[$i]['icon']['height'] = $aFileDimensions[1];
                            } else {
                                $aMarkerInfo[$i]['icon']['width'] = 32;
                            }

                            // case 2 > custom markers [file]
                            $aMarkerInfo[$i]['type'] = 2;
                            $aMarkerInfo[$i]['path'] = $sCustomMarkerPath;
                            $aMarkerInfo[$i]['icon']['scale'] = 0;
                            break;
                        default:
                            // case 1 > customizable markers
                            $aMarkerInfo[$i]['type'] = 0;
                            $aMarkerInfo[$i]['path'] = '';
                            $aMarkerInfo[$i]['icon']['width'] = 0;
                            $aMarkerInfo[$i]['icon']['height'] = 0;
                            $aMarkerInfo[$i]['icon']['scale'] = 0;
                            $aMarkerInfo[$i]['icon']['anchorX'] = 0;
                            $aMarkerInfo[$i]['icon']['anchorY'] = 0;
                            $aMarkerInfo[$i]['icon']['fillcolor'] = '';
                            $aMarkerInfo[$i]['icon']['fillopacity'] = 0;
                            $aMarkerInfo[$i]['icon']['strokecolor'] = '';
                            $aMarkerInfo[$i]['icon']['strokeopacity'] = 0;
                            $aMarkerInfo[$i]['icon']['strokewidth'] = 0;
                    }
                }

                $i++;
            }
        }

        // ### generate polygon array ###
        $aPolygons = [];
        $aPolygonsInfo = [];
        $sPolygons = '';
        $j = 0;

        // loop polygons
        foreach ($oItems as $oItem) {
            if ($oItem->getType() == 1) {
                $aPolygons[$j] = $oItem;

                $oItemPolygonPathsFile = $oItem->getPaths();
                $sItemPolygonPathsJSON = $oItemPolygonPathsFile ? $this->validateJSONString($oItemPolygonPathsFile) : FALSE;
                if($sItemPolygonPathsJSON){
                    if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
                        $sItemPolygonPathsFile = 'https://' . $_SERVER['HTTP_HOST'] . '/' . $oItemPolygonPathsFile->getOriginalResource()->getPublicUrl();
                    } else {
                        $sItemPolygonPathsFile = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $oItemPolygonPathsFile->getOriginalResource()->getPublicUrl();
                    }
                } else {
                    $sItemPolygonPathsFile = FALSE;
                }

                $aPolygonsInfo[$j]['paths'] = $sItemPolygonPathsFile;
                $aPolygonsInfo[$j]['strokeColor'] = $oItem->getStrokecolor();
                $aPolygonsInfo[$j]['strokeOpacity'] = (double)$oItem->getStrokeopacity();
                $aPolygonsInfo[$j]['strokeWeight'] = $oItem->getStrokewidth();
                $aPolygonsInfo[$j]['fillColor'] = $oItem->getFillcolor();
                $aPolygonsInfo[$j]['fillOpacity'] = (double)$oItem->getFillopacity();

                $j++;
            }
        }

        // done: convert arrays to json encoded strings for data attributes
        $sMapOptions = $this->getFormattedMapOptions($iMapZoom, $iMapMinZoom, $iMapMaxZoom, $fMapLat, $fMapLon, $sMapType, $sMapControls, $iMapControlSize, $bMapControlZoom, $sMapControlZoomPos, $bMapControlType, $sMapControlTypePos, $sMapControlTypeStyle, $bMapControlScale, $bMapControlStreetview, $sMapControlStreetviewPos, $bMapControlRotate, $sMapControlRotatePos, $bMapControlFullscreen, $sMapControlFullscreenPos, $bMapPanning, $bMapKeyboardshortcuts, $bMapDoubleclickzoom, $bMapScrollzoom, $bMapClickablepoints);
        $sMapClusterOptions = ($bMapCluster ? $this->getFormattedClusterOptions($iMapClusterGridsize, $iMapClusterMaxzoom, $bMapClusterZoomonclick, $bMapClusterAveragecenter, $iMapClusterMinclustersize, $sMapClusterStyles) : FALSE);
        
        // ### assign variables to template ###
        $this->view->assign('iRecordUid', $iRecordUid);
        $this->view->assign('iMapWidth', $iMapWidth . $sMapWidthUnit);
        $this->view->assign('iMapHeight', $iMapHeight . $sMapHeightUnit);

        $this->view->assign('sMapStylePath', $sMapStyleFilePath);
        $this->view->assign('sMapOptions', $sMapOptions);
        $this->view->assign('sMapClusterOptions', $sMapClusterOptions);
        $this->view->assign('iMapFitbounds', ($bMapFitBounds ? 1 : 0));
        $this->view->assign('iMapMarkerCluster', ($bMapCluster ? 1 : 0));
        $this->view->assign('iMapMarkerClusterType', ($iMapClusterType ? 1 : 0));

        $this->view->assign('aMarkers', ($aMarkerInfo ? json_encode($aMarkerInfo) : FALSE));
        $this->view->assign('aPolygons', ($aPolygonsInfo ? json_encode($aPolygonsInfo) : FALSE));

    }

    /**
     * get all options for a google map (configs)
     *
     * @param int $iMapZoom
     * @param int $iMapMinZoom
     * @param int $iMapMaxZoom
     * @param float $fMapLat
     * @param float $fMapLon
     * @param string $sMapType
     * @param string $sMapControls
     * @param string $iMapControlSize
     * @param bool $bMapControlZoom
     * @param string $sMapControlZoomPos
     * @param bool $bMapControlType
     * @param string $sMapControlTypePos
     * @param string $sMapControlTypeStyle
     * @param bool $bMapControlScale
     * @param bool $bMapControlStreetview
     * @param string $sMapControlStreetviewPos
     * @param bool $bMapControlRotate
     * @param string $sMapControlRotatePos
     * @param bool $bMapControlFullscreen
     * @param string $sMapControlFullscreenPos
     * @param bool $bMapPanning
     * @param bool $bMapKeyboardshortcuts
     * @param bool $bMapDoubleclickzoom
     * @param bool $bMapScrollzoom
     * @param bool $bMapClickablepoints
     *
     * @return string $sMapOptions
     */
    public function getFormattedMapOptions($iMapZoom, $iMapMinZoom, $iMapMaxZoom, $fMapLat, $fMapLon, $sMapType, $sMapControls, $iMapControlSize, $bMapControlZoom, $sMapControlZoomPos, $bMapControlType, $sMapControlTypePos, $sMapControlTypeStyle, $bMapControlScale, $bMapControlStreetview, $sMapControlStreetviewPos, $bMapControlRotate, $sMapControlRotatePos, $bMapControlFullscreen, $sMapControlFullscreenPos, $bMapPanning, $bMapKeyboardshortcuts, $bMapDoubleclickzoom, $bMapScrollzoom, $bMapClickablepoints)
    {

        $aMapOptions = [];
        if ($iMapZoom) {
            $aMapOptions['zoom'] = (integer)$iMapZoom;
        }
        if ($iMapMinZoom) {
            $aMapOptions['minZoom'] = (integer)$iMapMinZoom;
        }
        if ($iMapMaxZoom) {
            $aMapOptions['maxZoom'] = (integer)$iMapMaxZoom;
        }
        if ($fMapLat && $fMapLon) {
            $aMapOptions['center'] = [];
            $aMapOptions['center']['lat'] = (float)$fMapLat;
            $aMapOptions['center']['lng'] = (float)$fMapLon;
        }
        if ($sMapType) {
            $aMapOptions['mapTypeId'] = $sMapType;
        }

        // controls
        if ($sMapControls == "1") {
            // disable all ui elements
            $aMapOptions['disableDefaultUI'] = (boolean)'true';
        }
        if($sMapControls == "2"){
            // custom settings
            if ($iMapControlSize) {
                $aMapOptions['controlSize'] = $iMapControlSize;
            }
            $aMapOptions['zoomControl'] = $bMapControlZoom;
            $aMapOptions['mapTypeControl'] = $bMapControlType;
            $aMapOptions['scaleControl'] = $bMapControlScale;
            $aMapOptions['streetViewControl'] = $bMapControlStreetview;
            $aMapOptions['rotateControl'] = $bMapControlRotate;
            $aMapOptions['fullscreenControl'] = $bMapControlFullscreen;

            // set options
            if ($bMapControlZoom) {
                $aMapOptions['zoomControlOptions'] = [];
                $aMapOptions['zoomControlOptions']['position'] = $sMapControlZoomPos;
            }
            if ($bMapControlType) {
                $aMapOptions['mapTypeControlOptions'] = [];
                $aMapOptions['mapTypeControlOptions']['style'] = $sMapControlTypeStyle;
                $aMapOptions['mapTypeControlOptions']['position'] = $sMapControlTypePos;
            }
            if ($bMapControlStreetview) {
                $aMapOptions['streetViewControlOptions'] = [];
                $aMapOptions['streetViewControlOptions']['position'] = $sMapControlStreetviewPos;
            }
            if ($bMapControlRotate) {
                $aMapOptions['rotateControlOptions'] = [];
                $aMapOptions['rotateControlOptions']['position'] = $sMapControlRotatePos;
            }
            if ($bMapControlFullscreen) {
                $aMapOptions['fullscreenControlOptions'] = [];
                $aMapOptions['fullscreenControlOptions']['position'] = $sMapControlFullscreenPos;
            }
        }

        // behavior
        if($bMapPanning) {
            $aMapOptions['panControl'] = 'true';
        }
        if($bMapKeyboardshortcuts) {
            $aMapOptions['keyboardShortcuts'] = 'true';
        }
        if($bMapDoubleclickzoom) {
            $aMapOptions['disableDoubleClickZoom'] = 'true';
        }
        if($bMapScrollzoom) {
            $aMapOptions['scrollwheel'] = 'true';
        }
        if($bMapClickablepoints) {
            $aMapOptions['clickableIcons'] = 'true';
        }

        return json_encode($aMapOptions);
    }

    /**
     * get all options for a markerclusterer (configs)
     *
     * @param int $iMapClusterGridsize
     * @param int $iMapClusterMaxzoom
     * @param bool $bMapClusterZoomonclick
     * @param bool $bMapClusterAveragecenter
     * @param int $iMapClusterMinclustersize
     * @param string $sMapClusterStyles
     * @return string $sClusterOptions
     */
    public function getFormattedClusterOptions($iMapClusterGridsize, $iMapClusterMaxzoom, $bMapClusterZoomonclick, $bMapClusterAveragecenter, $iMapClusterMinclustersize, $sMapClusterStyles)
    {
        $aMapClusterOptions = [];
        if($iMapClusterGridsize != 0) { $aMapClusterOptions['gridSize'] = $iMapClusterGridsize; }
        if($iMapClusterMaxzoom != 0) { $aMapClusterOptions['maxZoom'] = $iMapClusterMaxzoom; }
        if($bMapClusterZoomonclick) { $aMapClusterOptions['zoomOnClick'] = $bMapClusterZoomonclick; }
        if($bMapClusterAveragecenter) { $aMapClusterOptions['averageCenter'] = $bMapClusterAveragecenter; }
        if($iMapClusterMinclustersize != 0) { $aMapClusterOptions['minimumClusterSize'] = $iMapClusterMinclustersize; }
        if($sMapClusterStyles != '') {
            $aMapClusterOptions['styles'] = $sMapClusterStyles;
        } else {
            $aMapClusterOptions['imagePath'] = "/typo3conf/ext/kitt3n_map/Resources/Public/Images/MarkerClusterer/m";
        }

        return json_encode($aMapClusterOptions);
    }

    /**
     * format RTE string to valid HTML
     *
     * @param $str
     * @return string $output
     */
    public function formatRTE($str) {
        $output = $this->configurationManager->getContentObject()->parseFunc($str, array(), '< lib.parseFunc_RTE');
        $output = str_replace('"', '\"', $output);
        $output = str_replace(["\n", "\r"], '', $output);
        // $output = nl2br($output);

        return $output;
    }

    /**
     * check if string or file is valid JSON format
     *
     * @param mixed $mJSON
     * @return bool|false|string
     */
    public function validateJSONString($mJSON)
    {
        if ($mJSON instanceof \TYPO3\CMS\Extbase\Domain\Model\FileReference) {
            $sValidation = file_get_contents($mJSON->getOriginalResource()->getPublicUrl());
        } else {
            $sValidation = $mJSON;
        }

        // check if string is valid JSON
        try {
            $bValidJSON = \GuzzleHttp\json_decode($sValidation);
            if ($bValidJSON !== NULL) {
                $sValidJSON = $sValidation;
                return $sValidJSON;
            } else {
                return FALSE;
            }
        } catch (ClientException $e) {
            $this->addFlashMessage(
                $sMessageBody = LocalizationUtility::translate('LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.controller.exception.client.body', 'kitt3n_map'),
                $sMessageTitle = LocalizationUtility::translate('LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.controller.exception.client.title', 'kitt3n_map'),
                $severity = \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR,
                $storeInSession = FALSE
            );
            return FALSE;
        } catch (RequestException $e) {
            $this->addFlashMessage(
                $sMessageBody = LocalizationUtility::translate('LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.controller.exception.request.body', 'kitt3n_map'),
                $sMessageTitle = LocalizationUtility::translate('LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.controller.exception.request.title', 'kitt3n_map'),
                $severity = \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR,
                $storeInSession = FALSE
            );
            return FALSE;
        } catch (\InvalidArgumentException $e) {
            $this->addFlashMessage(
                $sMessageBody = LocalizationUtility::translate('LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.controller.exception.invalid_argument.body', 'kitt3n_map'),
                $sMessageTitle = LocalizationUtility::translate('LLL:EXT:kitt3n_map/Resources/Private/Language/translation_db.xlf:tx_kitt3nmap_domain_model_item.controller.exception.invalid_argument.title', 'kitt3n_map'),
                $severity = \TYPO3\CMS\Core\Messaging\FlashMessage::ERROR,
                $storeInSession = FALSE
            );
            return FALSE;
        }
    }
}
